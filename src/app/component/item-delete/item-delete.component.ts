import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ProductService} from "../../service/product.service";

@Component({
  selector: 'app-item-delete',
  templateUrl: 'item-delete.component.html',
  styleUrls: ['item-delete.component.css']
})
export class ItemDeleteComponent implements OnInit {

  public static TYPE_PRODUCT: string = 'product';

  @Input() public TYPE: string;
  @Input() private dataObject: any;

  private mainTitle;
  private itemName;
  private msg;
  private dangerMsg;
  private cancelBtnLabel;
  private submitBtnLabel;
  private isDangerMsgAvailable: boolean = false;
  private shouldItemNameHighlited: boolean = false;

  constructor(private productService: ProductService, private activeModal: NgbActiveModal) { }

  ngOnInit() {

    if(this.TYPE === ItemDeleteComponent.TYPE_PRODUCT){
      this.mainTitle = 'Delete item';
      this.itemName = this.dataObject.name;
      this.msg = 'All information associated to this product will be permanently deleted.';
      this.dangerMsg = 'This operation can not be undone.';
      this.cancelBtnLabel = 'Cancel';
      this.submitBtnLabel = 'Delete';
      this.isDangerMsgAvailable = true;
      this.shouldItemNameHighlited = true;
    }
  }

  onSubmitPressed(){
    if(this.TYPE === ItemDeleteComponent.TYPE_PRODUCT){
      this.productService.deleteProduct(this.dataObject.id,
        suc => {
          console.log(suc);
          this.activeModal.close(suc);
          // this.router.navigate(['/my-products']);
        }, err => {
          console.log(err);
        }, httpErr => {
          console.log(httpErr);
        });
    }
  }

}
