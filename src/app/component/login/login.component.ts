import {Component, OnInit} from '@angular/core';
import {Router, ParamMap, ActivatedRoute} from "@angular/router";
import {map} from "rxjs/internal/operators";
import {Observable} from "rxjs";
import {AuthService} from "../../service/auth.service";
import {TokenService} from "../../service/token.service";
import {TokenResponse} from "../../model/token.response";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {

  public user = {
    username: '',
    password: ''
  };

  private sessionExpired = false;
  private loginError = false;
  private loginErrorMsg = '';
  private registered = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private tokenService: TokenService,
    private route: ActivatedRoute,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.userService.clearUserDetails();
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.sessionExpired = (params.get('state') === 's-ex') && this.tokenService.isTokenExpired();
      this.registered = (params.get('state') === 'rg-ok');
    });
  }

  onSuccess(user: any) {
    console.log(user);

    this.userService.saveCurrentUserDetails(user);

    this.router.navigate(['/my-products'])
  }


  login() {
    this.authService.login(this.user, user => {
      this.loginError = false;
      this.onSuccess(user);
    }, error => {
      console.log(error);
      this.loginError = true;
      this.loginErrorMsg = error.msg;
      this.user.username = '';
      this.user.password = '';
    }, httpError => {
      console.log(httpError);
      this.loginError = true;
      this.loginErrorMsg = httpError.error.error_description;
      this.user.username = '';
      this.user.password = '';
    });
  }
}
