import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../service/product.service";

@Component({
  selector: 'app-products',
  templateUrl: 'products.component.html',
  styleUrls: ['products.component.css']
})
export class ProductsComponent implements OnInit {

  private products = [];

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
    this.productService.getAllProducts(suc => {
      this.onSuccess(suc);
    }, error => {
      console.log(error);
    }, httpErr => {
      console.log(httpErr);
    });
  }

  onSuccess(response: any){
    this.products = response;
    console.log(response);
  }

  makeProductCountStr(productCount: number) {
    if (productCount < 1) {
      return "No items";
    } else if (productCount === 1) {
      return "1 item";
    } else {
      return productCount + " items";
    }
  }

}
