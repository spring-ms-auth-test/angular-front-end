import {Component, OnInit, Input} from '@angular/core';
import {ProductService} from "../../service/product.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-new-product',
  templateUrl: 'add-new-product.component.html',
  styleUrls: ['add-new-product.component.css']
})
export class AddNewProductComponent implements OnInit {

  @Input() isEdit : boolean = false;
  title = 'Add a new product';
  submitBtnLabel = 'Add';

  minCount = 1;

  @Input() product = {
    id: 0,
    name : '',
    count : ''
  };

  constructor(private productService: ProductService, private activeModal: NgbActiveModal) { }

  ngOnInit() {
    if(this.isEdit){
      this.title = 'Edit product';
      this.submitBtnLabel = 'Update';
    }
  }

  minErrorOccured(): boolean {

    if(this.product.count === ''){
      return false;
    }

    return +this.product.count < this.minCount;
  }

  onSubmitBtnClicked(){
    if(this.isEdit){
      this.updateProduct();
    } else {
      this.addProduct();
    }
  }

  addProduct(){
    this.productService.addProduct(this.product,
      suc => {
        console.log(suc);
        this.activeModal.close(suc);
        // this.router.navigate(['/my-products']);
      }, err => {
        console.log(err);
      }, httpErr => {
        console.log(httpErr);
      });
  }

  updateProduct(){
    this.productService.updateProduct(this.product,
      suc => {
        console.log(suc);
        this.activeModal.close(suc);
        // this.router.navigate(['/my-products']);
      }, err => {
        console.log(err);
      }, httpErr => {
        console.log(httpErr);
      });
  }

}
