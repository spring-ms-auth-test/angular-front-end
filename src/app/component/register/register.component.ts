import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../service/auth.service";

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.css']
})
export class RegisterComponent implements OnInit {

  userRegister = {};

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  register(){
    this.authService.register(this.userRegister).subscribe(
      res => this.router.navigate(['/login', {state: 'rg-ok'}]),
      error => console.log(error)
    );
  }
}
