import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../service/product.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AddNewProductComponent} from "../add-new-product/add-new-product.component";
import {ItemDeleteComponent} from "../item-delete/item-delete.component";

@Component({
  selector: 'app-my-products',
  templateUrl: 'my-products.component.html',
  styleUrls: ['my-products.component.css']
})
export class MyProductsComponent implements OnInit {

  private products = [];

  constructor(private productService: ProductService, private ngbModal: NgbModal) {
  }

  ngOnInit() {
    this.productService.getMyProducts(
      suc => {
        this.onSuccess(suc);
      }, err => {
        this.onError(err);
      }, httpErr => {
        this.onError(httpErr);
      });
  }

  onSuccess(response: any) {
    this.products = response;
    console.log(response);
  }

  onError(error: any) {
    console.log(error);
  }

  makeProductCountStr(productCount: number) {
    if (productCount < 1) {
      return "No items";
    } else if (productCount === 1) {
      return "1 item";
    } else {
      return productCount + " items";
    }
  }

  showAddProduct() {
    //noinspection TypeScriptUnresolvedFunction
    this.ngbModal.open(AddNewProductComponent, {centered: true}).result.then(result => {
        this.products.push(result);
      },
      reason => {
      });
  }

  editProduct(product) {
    const modalRef = this.ngbModal.open(AddNewProductComponent, {centered: true});
    //noinspection TypeScriptUnresolvedFunction
    modalRef.result.then(result => {
        let index = this.products.indexOf(product);
        this.products[index] = result;
      },
      reason => {
      }
    );

    modalRef.componentInstance.isEdit = true;
    modalRef.componentInstance.product = {
      id: product.id,
      name : product.name,
      count : product.count
    };
  }

  deleteProduct(product){
    const modalRef = this.ngbModal.open(ItemDeleteComponent, {centered: true});
    //noinspection TypeScriptUnresolvedFunction
    modalRef.result.then(result => {
        let index = this.products.indexOf(product);
        this.products.splice(index);
      },
      reason => {
      }
    );

    modalRef.componentInstance.TYPE = ItemDeleteComponent.TYPE_PRODUCT;
    modalRef.componentInstance.dataObject = product;
  }
}
