import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../service/auth.service";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {

  private isSidebarOpened: boolean = true;

  constructor(private authService: AuthService, private router: Router, private userService: UserService){}

  loggedIn(){
    return this.authService.loggedIn();
  }

  private toggleSidebar() {
    this.isSidebarOpened = !this.isSidebarOpened;
  }

  getUserFullName(){
    let user = this.userService.getCurrentUserDetails();
    if (user == null) {
      return '';
    }
    return user.fullName;
  }

  logOut(){
    this.authService.logOut();
    this.router.navigate(['/login'])
  }
}
