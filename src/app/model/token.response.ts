/**
 * Created by hasithar on 10/8/2018.
 */

export class TokenResponse {

  public access_token: string;
  public token_type: string;
  public refresh_token: string;
  public expires_in: number;
  public expires_at: number;
  public scope: string;

  constructor(){}

  public assign(obj) {
    this.access_token = obj.access_token;
    this.token_type = obj.token_type;
    this.refresh_token = obj.refresh_token;
    this.expires_in = obj.expires_in;
    this.scope = obj.scope;
  }

  public makeExpiresAt() {
    let time = (new Date()).getTime();
    this.expires_at = time + (this.expires_in * 1000);
  }
}
