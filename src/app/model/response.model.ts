/**
 * Created by hasithar on 10/11/2018.
 */
export class ResponseModel {

  public static STATUS_SUCCESS: number = 1000;
  public static STATUS_SERVER_ERROR: number = 5000;
  public static STATUS_NOT_FOUND: number = 4000;
  public static STATUS_INVALID_PARAMS: number = 4100;
}
