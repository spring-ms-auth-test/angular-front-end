import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './component/app/app.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { AddNewProductComponent } from './component/add-new-product/add-new-product.component';
import {AngularFontAwesomeModule} from "angular-font-awesome";
import { ItemDeleteComponent } from './component/item-delete/item-delete.component';
import {SidebarModule} from "ng-sidebar";


@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    AddNewProductComponent,
    ItemDeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    AngularFontAwesomeModule,
    SidebarModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AddNewProductComponent, ItemDeleteComponent]
})
export class AppModule { }
