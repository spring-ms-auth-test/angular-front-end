import {Injectable} from '@angular/core';
import {TokenResponse} from "../model/token.response";

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private TOKEN_RESPONSE_LABEL = "tokenResponse";

  constructor() {
  }

  public setTokenResponse(tokenResponse: TokenResponse) {
    console.log(tokenResponse);
    console.log("Set token : " + tokenResponse.access_token);
    localStorage.setItem(this.TOKEN_RESPONSE_LABEL, JSON.stringify(tokenResponse));
  }

  public getTokenResponse(): TokenResponse {
    return JSON.parse(localStorage.getItem(this.TOKEN_RESPONSE_LABEL));
  }

  public getToken(): string {
    let tokenResponse: TokenResponse = this.getTokenResponse();
    let token: string = '';
    if (tokenResponse) {
      token = tokenResponse.access_token;
    }

    console.log("Get token : " + token);

    return token;
  }

  public isTokenExpired(): boolean {
    let tokenResponse: TokenResponse = this.getTokenResponse();
    if (tokenResponse == null) {
      return true;
    }

    return tokenResponse.expires_at <= new Date().getTime();
  }

  public clearToken() {
    localStorage.removeItem(this.TOKEN_RESPONSE_LABEL);
  }

  public hasToken() {
    return !!localStorage.getItem(this.TOKEN_RESPONSE_LABEL);
  }
}
