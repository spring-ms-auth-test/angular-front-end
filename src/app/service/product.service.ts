import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthService} from "./auth.service";
import {AbstractService} from "./abstract.service";
import {TokenService} from "./token.service";
import {Router} from "@angular/router";
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ProductService extends AbstractService {

  private BASE_URL = "http://localhost:8082/product";
  private ALL_PRODUCTS_URL = this.BASE_URL + "/all";
  private MY_PRODUCTS_URL = this.BASE_URL + "/my-products";

  constructor(private http: HttpClient, private tokenService: TokenService, private router: Router) {
    super(http, tokenService, router);
  }

  getAllProducts(sucCallback, errCallback, httpErrorCallback?) {
    this.requestNormal(this.METHOD_GET, this.ALL_PRODUCTS_URL, null, null, sucCallback, errCallback, httpErrorCallback);
  }

  getMyProducts(sucCallback, errCallback, httpErrorCallback?) {
    this.requestTokenized(this.METHOD_GET, this.MY_PRODUCTS_URL, null, null, sucCallback, errCallback, httpErrorCallback);
  }

  addProduct(product, sucCallback, errCallback, httpErrorCallback?) {
    this.requestTokenized(this.METHOD_POST, this.BASE_URL, null, product, sucCallback, errCallback, httpErrorCallback);
  }

  updateProduct(product, sucCallback, errCallback, httpErrorCallback?) {
    this.requestTokenized(this.METHOD_PUT, this.BASE_URL, null, product, sucCallback, errCallback, httpErrorCallback);
  }

  deleteProduct(productId, sucCallback, errCallback, httpErrorCallback?) {
    this.requestTokenized(this.METHOD_DELETE, this.BASE_URL, {id: productId}, null, sucCallback, errCallback, httpErrorCallback);
  }
}
