import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private USER_LABEL = 'user';

  constructor() { }

  saveCurrentUserDetails(user: any) {
    localStorage.setItem(this.USER_LABEL, JSON.stringify(user));
  }

  getCurrentUserDetails() {
    return JSON.parse(localStorage.getItem(this.USER_LABEL));
  }

  public clearUserDetails() {
    localStorage.removeItem(this.USER_LABEL);
  }
}
