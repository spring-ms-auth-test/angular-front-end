import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {TokenResponse} from "../model/token.response";
import {TokenService} from "./token.service";
import {AbstractService} from "./abstract.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService extends AbstractService {

  protected BASE_URL = "http://localhost:8081";
  protected LOGIN_URL = this.BASE_URL + "/oauth/token";
  private REGISTER_URL = this.BASE_URL + "/user/register";
  private CURRENT_USER_URL = this.BASE_URL + "/user/current";

  constructor(private http: HttpClient, private tokenService: TokenService, private router: Router) {
    super(http, tokenService, router);
  }

  logOut() {
    this.tokenService.clearToken();
  }

  public loggedIn(): boolean {
    return this.tokenService.hasToken();
  }

  register(userData): Observable<any> {
    return this.http.post<any>(this.REGISTER_URL, userData)
  }

  login(user: {username: string; password: string}, sucCallback, errCallback, httpErrorCallback?) {
    let clientId = 'my-trusted-client';
    let secret = 'secret';
    let basicHeader = btoa(clientId + ':' + secret);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + basicHeader
      })
    };


    let data = 'grant_type=password&client_id=my-trusted-client&username=' + user.username + '&password=' + user.password;

    this.http.post(this.LOGIN_URL, data, httpOptions).subscribe(
      res => {
        let response: TokenResponse = new TokenResponse();
        response.assign(res);
        response.makeExpiresAt();
        console.log(response);
        this.tokenService.setTokenResponse(response);

        this.getCurrentUser(sucCallback, errCallback, httpErrorCallback)
      },
      errCallback,
    );
  }

  getCurrentUser(sucCallback, errCallback, httpErrorCallback) {
    this.requestTokenized('GET', this.CURRENT_USER_URL, null, null, sucCallback, errCallback, httpErrorCallback);
  }
}
