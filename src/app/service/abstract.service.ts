import {Injectable} from '@angular/core';
import {TokenService} from "./token.service";
import {HttpClient} from "@angular/common/http";
import {HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {TokenResponse} from "../model/token.response";
import {Router} from "@angular/router";
import {ResponseModel} from "../model/response.model";

@Injectable({
  providedIn: 'root'
})
export class AbstractService {

  protected METHOD_POST : string = "POST";
  protected METHOD_GET : string = "GET";
  protected METHOD_PUT : string = "PUT";
  protected METHOD_DELETE : string = "DELETE";

  protected AUTH_URL = "http://localhost:8081/oauth/token";

  constructor(private _http: HttpClient, private _tokenService: TokenService, private _router: Router) {
  }

  protected static onSuccessCallback(response, sucCallback, errCallback){
    if(response.code === 1000){
      sucCallback(response.data);
    } else {
      errCallback(response);
    }
  }

  protected requestNormal(method: string, url: string, params: any, data: any, sucCallback, errCallback, httpErrorCallback?) {
    let options = {
      body: data,
      params: params
    };

    let request: Observable<any> = this._http.request(method, url, options);
    return request.subscribe(
      response => {
        AbstractService.onSuccessCallback(response, sucCallback, errCallback);
      },
      httpErrorCallback
    );
  }

  protected requestTokenized(method: string, url: string, params: any, bodyData: any, sucCallback, errCallback, httpErrorCallback?) {
    let options = {
      body: bodyData,
      headers: this.makeAuthHeaders(),
      params: params
    };

    let request: Observable<any> = this._http.request(method, url, options);
    return request.subscribe(
      response => {
        AbstractService.onSuccessCallback(response, sucCallback, errCallback);
      },
      error => {
        if (error.status === 401) {
          this.refreshToken(method, url, bodyData, sucCallback, errCallback, httpErrorCallback);
        } else {
          httpErrorCallback(error);
        }
      }
    );
  }

  refreshToken(method: string, url: string, bodyData: any, sucCallback, errCallback, httpErrorCallback) {
    console.log("Refreshing token");

    let clientId = 'my-trusted-client';
    let secret = 'secret';
    let basicHeader = btoa(clientId + ':' + secret);

    const authHttpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + basicHeader
      })
    };

    let authData = 'grant_type=refresh_token&refresh_token=' + this._tokenService.getTokenResponse().refresh_token;

    return this._http.post(this.AUTH_URL, authData, authHttpOptions).subscribe(
      res => {
        let response: TokenResponse = new TokenResponse();
        response.assign(res);
        response.makeExpiresAt();
        console.log(response);
        this._tokenService.setTokenResponse(response);

        let options = {
          body: bodyData,
          headers: this.makeAuthHeaders()
        };
        let request: Observable<any> = this._http.request(method, url, options);
        return request.subscribe(
          response => {
            AbstractService.onSuccessCallback(response, sucCallback, errCallback);
          },
          httpErrorCallback
        );
      },

      error => {
        this._tokenService.clearToken();
        this._router.navigate(['/login', {state: 's-ex'}]);
        httpErrorCallback(error);
      }
    );

  }

  makeAuthHeaders() {
    // return {
    //   headers: new HttpHeaders({
    //     'Authorization': 'Bearer ' + this.tokenService.getToken()
    //   })
    // };

    return {
      'Authorization': 'Bearer ' + this._tokenService.getToken()
    };
  }
}
